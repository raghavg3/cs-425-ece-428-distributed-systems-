import time
import random
import socket
import threading
import sys
import json
import hashlib
import netifaces as ni # need pip3 install
from collections import defaultdict
import traceback
import copy
import pickle


SERIVCE_IP, SERIVCE_PORT = ('172.22.94.168', '8888')

class Block:
    def __init__(self, txs, prev_hash, puzzle, solution, height):
        self.txs = txs
        self.prev_hash = prev_hash
        self.solution = solution
        self.puzzle = puzzle
        self.height = height
        # puzzle = prev_hash + txs
        # solution = hash(puzzle + solution) st < 2 ^ -k
    def to_json(self):
        return {"prev_hash":self.prev_hash, "solution":self.solution, "height":self.height}

class StateMachine:
    def __init__(self):
        self.status = defaultdict(int)
        self.status[0] = float('inf')

    def update_status(self, block):
        for tx in block.txs:
            _, t, h, src, dest, amount = tx.strip().split()
            self.status[int(src)] -= int(amount)
            self.status[int(dest)] += int(amount)
        print("#[Info] Update account balance: {}\n".format(str(self.status)))

    def set_ledger(self, d):
        self.status = d

    def get_summary(self):
        return self.status

    def verify_status(self, txs):
        # check if selected txs are executable
        # return executable txs
        newset_txs = []
        tmp_status = copy.deepcopy(self.status)
        for tx in txs:
            _, t, h, src, dest, amount = tx.strip().split()
            if tmp_status[int(src)] >= int(amount):
                newset_txs.append(tx)
                tmp_status[int(src)] -= int(amount)
                tmp_status[int(dest)] += int(amount)         
        return newset_txs


class Application:
    def __init__(self, name, port, log=False, threshold=2):
        self.name = name
        self.ip = socket.gethostbyname(socket.gethostname())
        self.port = port
        self.nodes = set()
        self.shutdown = False
        
        # recv = unverify + verify for all the time
        self.transactions = []
        self.unverified_transactions = []


        self.service_socket = None 
        
        self.transaction_recv_time = defaultdict()
        self.transactions_in_block_time = defaultdict()
        self.block_recv_time = defaultdict()
        self.bandwidth = 0
        self.bandwidth_dict = defaultdict()
        self.start_time = time.time()
        self.split_count = 0
        self.split_count_dict = defaultdict()
        self.output_blocks = defaultdict()

        # state machine
        self.state_machine = StateMachine()
        self.mining_txs = []
        self.wait_for_solution = False
        self.verifing_blocks = None
        self.verifing_ledger = None
        self.solving_block = None
        self.height = 0
        
        
        self.blocks = defaultdict()
        self.current = '0'
        

    def logger(self):
        while True:
            time.sleep(20)
            fname = './{}_block_propagation.txt'.format(self.name)
            with open(fname, 'w+') as f:
                f.write(json.dumps(self.block_recv_time))
                f.flush()

            fname = './{}_blocks.txt'.format(self.name)
            with open(fname, 'w+') as f:
                f.write(json.dumps(self.output_blocks))
                f.flush()

            fname = './{}_tx_inblock_time.txt'.format(self.name)
            with open(fname, 'w+') as f:
                f.write(json.dumps(self.transactions_in_block_time))
                f.flush()

            fname = './{}_bandwidth.txt'.format(self.name)
            self.bandwidth_dict[time.time()] = round(self.bandwidth/(time.time() - self.start_time), 2)
            with open(fname, 'w+') as f:
                f.write(json.dumps(self.bandwidth_dict))
                f.flush()

            fname = './{}_split.txt'.format(self.name)
            self.split_count_dict[time.time()] = self.split_count
            with open(fname, 'w+') as f:
                f.write(json.dumps(self.split_count_dict))
                f.flush()

            fname = './{}_tx_recv_time.txt'.format(self.name)
            with open(fname, 'w+') as f:
                f.write(json.dumps(self.transaction_recv_time))
                f.flush()


    def init_connection(self):
        try:
            req = "CONNECT {} {} {}\n".format(self.name, self.ip, self.port)
            self.service_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            #self.service_socket.setsockopt(socket.SOL_SOCKET, socket.SO_SNDBUF, 2 ** 13)
            self.service_socket.connect((SERIVCE_IP, int(SERIVCE_PORT)))
            threading.Thread(target=self.listen_to_client, args=(self.service_socket, SERIVCE_IP, SERIVCE_PORT)).start()
            self.service_socket.sendall(req.encode('utf-8'))
        except Exception as e:
            print("[Error] Init error: {}, Line: {}".format(e, sys.exc_info()[-1].tb_lineno))
        
    def recv_connection(self):
        recv_s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #recv_s.setsockopt(socket.SOL_SOCKET, socket.SO_RCVBUF, 2 ** 13)
        recv_s.bind((self.ip, int(self.port)))
        recv_s.listen(200)
        while True:
            try:
                client_socket, addr = recv_s.accept()
                #print('#Recv connection from {}:{}\n'.format(addr[0], addr[1]))
                t = threading.Thread(target=self.listen_to_client, args=(client_socket, addr[0], addr[1]))
                t.start()
            except Exception as e:
                print("#[Error] recv connection error: {}, Line: {}\n".format(e, sys.exc_info()[-1].tb_lineno)) 

    def listen_to_client(self, client_socket, client_ip, client_port):
        size = 8192
        while True:
            try:
                received_data = client_socket.recv(size)
                if not received_data:
                    break
                m = received_data.decode('utf-8')
                for msg in m.split('\n'):
                    command = msg.split(' ')[0]
                    if command == 'DIE':
                        print('#Receive DIE\n')
                        os._exit(0)

                    elif command == 'INTRODUCE':
                        _, name, ip, port = msg.split(' ')
                        if (ip, port) not in self.nodes:
                            self.nodes.add((ip, port))

                    elif command == 'TRANSACTION':
                        if len(msg.split()) != 6:
                            continue
                        if msg not in self.transactions:
                            _, t_s, t_id, *rest = msg.split()
                            new_id = t_id + ' ' + t_s
                            self.transaction_recv_time[new_id] = time.time() - float(t_s)
                            #print('#[Info] recv tx from {}:{}\n'.format(client_ip, client_port))
                            self.transactions.append(msg)
                            self.unverified_transactions.append(msg)
                            self.gossip_txs_blocks(msg+'\n')

                    elif command == 'SOLVED':
                        '''
                        Todo:
                        1. Construct the block
                        2. Remove the transactions from the mempool
                        3. Broadcast blocks
                        '''
                        _, puzzle, solution = msg.strip().split()
                        if self.solving_block is not None:
                            self.solving_block.solution = solution

                            if solution not in self.blocks:
                                self.blocks[solution] = copy.deepcopy(self.solving_block)
                                self.output_blocks[solution] = copy.deepcopy(self.solving_block.to_json())
                                self.block_recv_time[solution] = time.time()
                                self.tx_in_block_time(solution, self.solving_block)

                                if self.solving_block.height == self.height+1:
                                    self.state_machine.update_status(self.solving_block)
                                    self.height = self.solving_block.height
                                    self.current = solution
                                
                                    blocks_string = "BLOCKS {} {}\n".format(pickle.dumps(self.solving_block, protocol=4).hex(),
                                        pickle.dumps(self.state_machine.status, protocol=4).hex())
                                    self.gossip_txs_blocks(blocks_string)
                                    self.solving_block = None
                                
                                    self.update_mining_pool()

                        self.wait_for_solution = False

                    elif command == 'BLOCKS':
                        '''
                        Todo:
                        If the received height is larger than current height
                        1. cancel the current solving block
                        2. verify new blocks
                        '''
                        if len(msg.strip().split()) != 3:
                            continue
                        _, data, ledger = msg.strip().split()
                        try:
                            recv_block = pickle.loads(bytes.fromhex(data))
                            ledger = pickle.loads(bytes.fromhex(ledger))
                            if self.verifing_blocks is None:
                                self.verifing_blocks = recv_block
                                self.verifing_ledger = ledger
                                puzzle, solution = recv_block.puzzle, recv_block.solution
                                self.service_socket.sendall('VERIFY {} {}\n'.format(puzzle, solution).encode('utf-8'))
                        except Exception as e:
                            continue

                        
            
                    elif command == 'VERIFY':
                        '''
                        Only need to verify when other nodes send broadcast blocks
                        Todo:
                        1. update the verified blocks
                        2. update txs in mining pool
                        3. allow to create new puzzle based on the current mining pool
                        '''
                        command, result, puzzle, solution = msg.strip().split()
                        if result == 'OK':
                            if solution not in self.blocks:
                                self.blocks[solution] = self.verifing_blocks
                                self.output_blocks[solution] = self.verifing_blocks.to_json()
                                self.block_recv_time[solution] = time.time()
                                self.tx_in_block_time(solution, self.verifing_blocks)

                                if self.verifing_blocks.height > self.height:
                                    if self.current != self.verifing_blocks.prev_hash:
                                        self.split_count += 1
                                    self.height = self.verifing_blocks.height
                                    self.current = self.verifing_blocks.solution
                                    self.state_machine.set_ledger(self.verifing_ledger)
                                    self.update_mining_pool()
                                    blocks_string = "BLOCKS {} {}\n".format(pickle.dumps(self.verifing_blocks, protocol=4).hex(),
                                        pickle.dumps(self.state_machine.status, protocol=4).hex())
                                    self.gossip_txs_blocks(blocks_string)
                            
                            self.verifing_blocks = None
                            self.verifing_ledger = None
                            self.wait_for_solution = False
                        else:
                            self.verifing_blocks = None
                            self.wait_for_solution = False


            except Exception as e:
                print('#[Error] recv error: {}, Line: {}\n'.format(e, sys.exc_info()[-1].tb_lineno))
        client_socket.close()

    def mining(self):
        while True:
            # each block contains at least 20 txs
            if not self.wait_for_solution and len(self.unverified_transactions) > 10:
                sub_txs = random.sample(self.unverified_transactions, min(len(self.unverified_transactions), 2000))
                self.mining_txs = self.state_machine.verify_status(sub_txs)
                prev_hash = '0' if len(self.blocks) == 0 else self.current
                puzzle = prev_hash + pickle.dumps(self.mining_txs).hex()
                puzzle = hashlib.sha256(str.encode(puzzle)).hexdigest()
                height = self.height + 1
                self.solving_block = Block(copy.deepcopy(self.mining_txs), prev_hash, puzzle, None, height)
                puzzle_msg = "SOLVE {}\n".format(puzzle)
                self.service_socket.sendall(puzzle_msg.encode('utf-8'))
                self.wait_for_solution = True

    def gossip_txs_blocks(self, data):
        gossip_node = set(random.sample(self.nodes, min(3, len(self.nodes)))) # OR: must send to 3 nodes successfully
        for i in gossip_node:
            self.send_node(i, data)

    def gossip_node(self):
        while True:
            time.sleep(10)
            nd_set = set()
            nd_set.add("INTRODUCE node {} {}\n".format(self.ip, self.port))
            known_nd = set(random.sample(self.nodes, min(2, len(self.nodes)))) # OR: must send to 3 nodes successfully
            for nd in known_nd:
                nd_set.add("INTRODUCE node {} {}\n".format(nd[0], nd[1]))
            
            gossip_node = set(random.sample(self.nodes, min(3, len(self.nodes))))
            for i in gossip_node:
                for nd in nd_set:
                    self.send_node(i, nd)

    def send_node(self, node, msg):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.settimeout(120)
            self.bandwidth += len(msg)
            s.connect((node[0], int(node[1])))
            s.sendall(msg.encode('utf-8'))
        except Exception as e:
            #if e[0] == errno.ECONNRESET:
                #print("#[Error] send node {}, Line: {}\n".format(e, sys.exc_info()[-1].tb_lineno))
            if node in self.nodes:
                self.nodes.remove(node)
                print("#[Info] Disconnect: {}\n".format(str(node)))
        s.close()

    def update_mining_pool(self):
        tmp = copy.deepcopy(self.current)
        v = []
        try:
            while tmp != '0':
                if tmp in self.blocks:
                    b = self.blocks[tmp]
                    v.extend(b.txs)
                    tmp = b.prev_hash
                else:
                    break
            self.unverified_transactions = list(set(self.transactions) - set(v))
        except Exception as e:
            print('#[Error] update_mining_pool error: {}, Line: {}\n'.format(e, sys.exc_info()[-1].tb_lineno)) 

    def tx_in_block_time(self, block_hash, block):
        try:
            t_recv = self.block_recv_time[block_hash]
            for tx in block.txs:
                _, t_s, t_id, *rest = tx.split()
                new_id = t_id + ' ' + t_s
                if new_id not in self.transactions_in_block_time and new_id in self.transaction_recv_time:
                    self.transactions_in_block_time[new_id] = t_recv - float(t_s)
        except Exception as e:
            print('#[Error] tx_in_block_time error: {}, Line: {}\n'.format(e, sys.exc_info()[-1].tb_lineno))

    def run(self):
        init_recv_thread = threading.Thread(target=self.recv_connection)
        init_recv_thread.start()
        init_connection_thread = threading.Thread(target=self.init_connection)
        init_connection_thread.start()
        init_connection_thread.join()
        mining_thread = threading.Thread(target=self.mining)
        mining_thread.start()
        gossip_node_thread = threading.Thread(target=self.gossip_node)
        gossip_node_thread.start()
        logger_thread = threading.Thread(target=self.logger)
        logger_thread.start()
        while not self.shutdown:
            if self.shutdown:
                # if self.log_flag:
                #     with open('./{}.txt'.format(self.name), 'w+') as f:
                #         f.write(json.dumps(self.transaction_recv_time))
                #         f.flush() 
                os._exit(0)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print("Usage: python36 mp2.py name port")
        sys.exit(0)
    a = Application(sys.argv[1], sys.argv[2], log=True)
    a.run()

