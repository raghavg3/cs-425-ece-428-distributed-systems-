import os
import sys

if __name__ == "__main__":
    # python36 run_multi_client.py 1
    interval = 20
    N_node_start = (int(sys.argv[1])-1)*interval
    N_node_end = int(sys.argv[1])*interval
        
       
    num = [str(i) for i in range(N_node_start+1,N_node_end+1)]
    ports = [str(8000+i) for i in range(N_node_start+1,N_node_end+1)] 
    command = []
    for i in range(interval):
        command.append(f"python36 mp2cp2.py node{num[i]} {ports[i]}")

    total_comm = ' & \n'.join(command) +' &'
    print(total_comm)
    os.system(total_comm)
