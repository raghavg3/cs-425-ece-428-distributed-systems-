import numpy as np
import matplotlib.pyplot as plt 
import statistics
import json
max_list,min_list,median_list=[],[],[]
misc=[]


flag=False

random_bw_num=0
bd_log = json.load(open('node1_bandwidth.txt', 'r'))
sorted_bd = sorted(bd_log.items(), key=lambda x:x[0])
t = [x[0] for x in sorted_bd]
bd = [x[1] for x in sorted_bd]
plt.plot(t,bd)
plt.show()



node_dict = {}
for i in range(2):
	node_dict[i] = json.load(open('node{}.txt'.format(i), 'r'))    
#print(node_dict)
service_log=json.load(open('service_log.txt'))
service_log=sorted(service_log.items(), key=lambda x : -x[1])
print(len(service_log))

for x, (k, v) in enumerate(service_log):
    
    #k = np.random.choice(list(service_log.keys()), 1)[0]
    if x == 191:
        print(k)
    l = []
    for i in range(2):
        if k in node_dict[i].keys():
            l.append(node_dict[i][k] - v)
        if len(l)== 2:
            misc.append(x)
            max_val=max(l)
            max_list.append(max_val)
            min_val=min(l)
            min_list.append(min_val)
            median_val=statistics.median(l)		
            median_list.append(median_val)
if misc==list(range(misc[0],misc[-1])):
    flag=True
    
print(set(list(range(misc[0]+1,misc[-1]))) - set(misc))
            
print(min_list)
print(flag)

plt.plot(misc,max_list)
plt.plot(misc,min_list)
plt.plot(misc,median_list)
plt.show()