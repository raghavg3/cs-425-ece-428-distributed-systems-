import time
import random
import socket
import threading
import sys
import json
import hashlib
import netifaces as ni # need pip3 install
from collections import defaultdict
import traceback

SERIVCE_IP, SERIVCE_PORT = ('172.22.94.168', '8888')
class Application:
    def __init__(self, name, ip, port, log=False, threshold=7):
        self.INFECTED = 1
        self.UNINFECTED = -1
        self.name = name
        self.ip = ip
        #self.ip = ni.ifaddresses('eth0')[ni.AF_INET][0]['addr']
        self.port = port
        self.status = self.UNINFECTED
        self.sending_group = defaultdict()
        # use priority queue with priority = -t?
        self.received_transaction = []
        self.service_socket = None 
        self.recv_send_mapping = defaultdict()
        self.shutdown = False
        self.log_flag = log
        self.transaction_recv_time = defaultdict()
        self.reference_dict=defaultdict()
        self.threshold = threshold
        self.query_period = 10

    def init_connection(self):
        try:
            req = "CONNECT {} {} {}\n".format(self.name, self.ip, self.port)
            self.service_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.service_socket.connect((SERIVCE_IP, int(SERIVCE_PORT)))
            threading.Thread(target=self.listen_to_client, args=(self.service_socket, SERIVCE_IP, SERIVCE_PORT)).start()
            self.service_socket.send(req.encode('utf-8'))
        except Exception as e:
            print(e, sys.exc_info())

    def connect_to_host(self, ip, port):
        try:
            send_s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            send_s.connect((ip, int(port)))
            print('# Successfully connect to {}'.format(ip))
            self.sending_group[(ip, port)] = send_s
        except:
            print('# Fail to connect {}:{}'.format(ip, port))
        
    def recv_connection(self):
        recv_s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        recv_s.bind((self.ip, int(self.port)))
        recv_s.listen(40)
        while not self.shutdown:
            try:
                client_socket, addr = recv_s.accept()
                print('# Recv connection from {}:{}'.format(addr[0], addr[1]))
                t = threading.Thread(target=self.listen_to_client, args=(client_socket, addr[0], addr[1]))
                t.setDaemon(True)
                t.start()
            except Exception as e:
                print(e, sys.exc_info())
                

    def listen_to_client(self, client_socket, client_ip, client_port):
        size = 2048
        while not self.shutdown:
            try:
                received_data = client_socket.recv(size)
                if received_data:
                    m = received_data.decode('utf-8').strip()
                    for msg in m.split('\n'):
                        msg = msg.replace(',', '')
                        command = msg.split(' ')[0]
                        if command in ['QUIT', 'DIE']:
                            print('Receive DIE')
                            self.shutdown = True
                            break

                        elif command == 'INTRODUCE':
                            #print('# Get introduce: {}'.format(msg))
                            for s in msg.split('\n'):
                                _, name, ip, port = s.split(' ')
                                if (ip, port) not in self.sending_group.keys() and (ip, port) != (self.ip, self.port):
                                    self.connect_to_host(ip, port)
                                    # Introduce yourself
                                    self.sending_group[(ip, port)].send("SELFINTRODUCE {} {} {}\n".format(self.name, self.ip, self.port).encode('utf-8'))
                                    self.query_period /= 2
                                else:
                                    self.query_period = min(self.query_period+2, 300)
                                

                        elif command == 'SELFINTRODUCE':
                            #print('# Get selfintroduce: {}'.format(msg))
                            _, name, ip, port = msg.split(' ')
                            #print('in self-intro', client_ip, client_port, ip, port)
                            if (ip, port) not in self.sending_group.keys() and (ip, port) != (self.ip, self.port):
                                #print('!!! connect by SELFINTRODUCE {}:{}'.format(ip, port))
                                self.connect_to_host(ip, port)
                                self.sending_group[(ip, port)].send("SELFINTRODUCE {} {} {}\n".format(self.name, self.ip, self.port).encode('utf-8'))
                            if (client_ip, client_port) not in self.recv_send_mapping.keys():
                                self.recv_send_mapping[(client_ip, client_port)] = (ip, port)

                        elif command == 'TRANSACTION':
                            #print(len(self.sending_group), self.threshold)
                            if self.status == self.UNINFECTED:
                                self.status = self.INFECTED
                            if msg not in self.received_transaction:
                                print('# Get transaction from: {}:{}\n'.format(client_ip, client_port))
                                self.received_transaction.append(msg)
                            if len(self.sending_group) >= self.threshold:
                                if msg not in self.transaction_recv_time.keys():
                                    
                                    self.transaction_recv_time[msg] = time.time()
                                    msgs=msg.split(' ')
                                    new_msg=msgs[2]
                                    self.reference_dict[new_msg] = time.time()
                                    with open('./{}.txt'.format(self.name), 'w+') as f:
                                        f.write(json.dumps(self.reference_dict))
                                        f.flush()
                                #print(len(self.transaction_recv_time))
                        
                        elif command == 'QUERYHOST':
                            for (port, ip) in self.sending_group.keys():
                                self.sending_group[self.recv_send_mapping[(client_ip, client_port)]].send('INTRODUCE {} {} {}\n'.format('_', port, ip).encode('utf-8'))

                    # elif command == 'RESPHOST':
                    #     print('# Recv RESPHOST, get: {}'.format(msg.split(' ')[1]))
                    #     self.update_hosts(Parser.string_to_list(msg.split(' ')[1]))

            except Exception as e:
                print('Node {}:{} receiving socket leave\n'.format(client_ip, client_port))
                print(e, sys.exc_info())
                break

    def gossip_send(self):
        while self.status and not self.shutdown:
            try:
                #print('# Start gossip', self.sending_group.keys())
                time.sleep(5)
                num_node = min(3, len(self.sending_group))
                send_idx = random.sample(list(self.sending_group.keys()), num_node)
                
                for idx in send_idx:
                    try:
                        to_send_msg = self.received_transaction[::-1][:50]
                        print('Sending {} messages to {} nodes\n'.format(str(len(to_send_msg)), str(num_node)))
                        #print('# Send to {}'.format(str(send_idx)))
                        bandwidth=((len(to_send_msg)*num_node)*80*8)/5
                        print('bandwidth is {} bits per second'.format(bandwidth))
                        for m in to_send_msg:
                            m += '\n'
                            self.sending_group[idx].send(m.encode('utf-8'))
                    except:
                       self.sending_group.pop(idx)
                       print('Node {}:{} sending socket leave\n'.format(idx[0], idx[1]))
            except Exception as e:
                print("# Error at gossip", e, sys.exc_info())

    def query_hosts(self):
        while not self.shutdown:
            try:
                time.sleep(self.query_period)
                for s in self.sending_group.values():
                    s.send('QUERYHOST\n'.encode('utf-8'))
            except Exception as e:
                print(e, sys.exc_info())

    def run(self):
        init_recv_thread = threading.Thread(target=self.recv_connection)
        init_recv_thread.setDaemon(True)
        init_recv_thread.start()
        init_connection_thread = threading.Thread(target=self.init_connection)
        init_connection_thread.start()
        init_connection_thread.join()
        gossip_thread = threading.Thread(target=self.gossip_send)
        gossip_thread.setDaemon(True)
        gossip_thread.start()
        query_thread = threading.Thread(target=self.query_hosts)
        query_thread.setDaemon(True)
        query_thread.start()
        while not self.shutdown:
            if self.shutdown:
                # if self.log_flag:
                #     with open('./{}.txt'.format(self.name), 'w+') as f:
                #         f.write(json.dumps(self.transaction_recv_time))
                #         f.flush() 
                sys.exit()


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print("Usage: python36 mp2.py name ip port")
        sys.exit(0)
    a = Application(sys.argv[1], sys.argv[2], sys.argv[3], log=True)
    a.run()

