import socket
import threading
import sys

CORD_IP, CORD_PORT = ('172.22.94.168', 8888)
class Client(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.tx_started = False
        self.wait_for_resp = False

    def get_input(self):
        while True:
            cmd = input()
            cmd_list = cmd.split(" ")

            if len(cmd_list) < 1:
                print("No Command\n")
            else:
                com = cmd_list[0]

                if com == "BEGIN":
                    if not self.tx_started:
                        self.tx_started = True
                        self.send_msg(CORD_IP, CORD_PORT, cmd)
                    else:
                        print("Invalid BEGIN\n")

                elif com == "SET":
                    if not self.tx_started:
                        print("Please begin a trainsaction\n")
                    elif len(cmd_list) != 3:
                        print("Invalid SET\n")
                    else:
                        server_node = cmd_list[1].split(".")[0]
                        if server_node in ['A', 'B', 'C', 'D', 'E']:
                            self.send_msg(CORD_IP, CORD_PORT, cmd)
                        else:
                            print("Invalid Server\n")

                elif com == "GET":
                    if not self.tx_started:
                        print("Please begin a transaction\n")
                    elif len(cmd_list) != 2:
                        print("Invalid GET\n")
                    else:
                        server_node = cmd_list[1].split(".")[0]
                        if server_node in ['A', 'B', 'C', 'D', 'E']:
                            self.send_msg(CORD_IP, CORD_PORT, cmd)
                        else:
                            print("Invalid Server\n")

                elif com in ["COMMIT", "ABORT"]:
                    if not self.tx_started:
                        print("Please Begin a Trainsaction\n")
                    else:
                        self.send_msg(CORD_IP, CORD_PORT, cmd)

                else:
                    print("Invalid Command\n")

    def send_msg(self, host, port, cmd):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.connect((host, port))
        except:
            s.close()

        try:
            s.sendall(cmd.encode('utf-8'))
        except:
            s.close()

        s.close()

    def receive_cmd(self):
        ss = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        ss.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        ss.bind((self.host, self.port))
        ss.listen(100)

        while True:
            conn, addr = ss.accept()
            while True:
                recv_data = conn.recv(4096)
                if not recv_data:
                    break

                recv_data_list = recv_data.decode('utf-8').split(":")

                if recv_data_list[0] == "TID":
                    print("OK")

                elif recv_data_list[0] == "Aborted":
                    print("ABORTED")
                    self.tx_started = False

                elif recv_data_list[0] == "Committed":
                    print("COMMIT OK")
                    self.tx_started = False

                elif recv_data_list[0] == "GOT": 
                    cmd = recv_data_list[2]
                    if "=" in cmd:
                        val = cmd.split("=")[1].strip()
                        obj = cmd.split("=")[0].strip()
                        if val != "Wait":
                            print(obj + " = " + val)
                        else:
                            print("# {} Wait".format(obj))
                        
                    else:
                        print('worng msg in got', recv_data)

                elif recv_data_list[0] == "NOT FOUND":
                    TID = recv_data_list[1]
                    self.tx_started = False
                    print("ABORTED")

                elif recv_data_list[0] == "SETTED": 
                    set_result = recv_data_list[2]
                    if set_result == "OK":
                        print("OK")
                    elif set_result == "Wait":
                        print("#Wait")
                    else:
                        print('SET, receive worng msg')

                else:
                    print('Wrong msg\n')

            conn.close()

if __name__ == '__main__':
    host = socket.gethostbyname(socket.gethostname())
    client_node = Client(host, 9999)
    t1 = threading.Thread(target=client_node.get_input)
    t2 = threading.Thread(target=client_node.receive_cmd)
    t1.daemon = True
    t2.daemon = True
    t2.start()
    t1.start()

    while True:
        pass