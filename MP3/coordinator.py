import socket
import threading
import time
import sys
from copy import deepcopy

SERVER_IP_DICT = {
    'A':'172.22.156.159',
    'B':'172.22.158.159',
    'C':'172.22.94.169',
    'D':'172.22.156.160',
    'E':'172.22.158.160'
}

class Coordinator(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.TID = 0
        self.IP_TID_DICT = {} 
        self.TID_SERVER_DICT = {} 
        self.TID_VOTES = {} 
        self.SERVER_LIST = []
        self.WAIT_GRAPH_REC = []

    def multicast(self, TID, cmd):
        for host in self.TID_SERVER_DICT[TID]:
            self.send_msg(host, 8888, cmd)

    def multicast_wait_graph(self, cmd):
        for ss in ['A', 'B', 'C', 'D', 'E']:
            self.send_msg(SERVER_IP_DICT[ss], 8888, cmd)

    def send_msg(self, host, port, cmd):
        sending_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            sending_socket.connect((host, port))
        except:
            sending_socket.close()
        try:
            sending_socket.sendall(cmd.encode('utf-8'))
        except:
            sending_socket.close()
        sending_socket.close()

    def receive_cmd(self):
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_socket.bind((self.host, self.port))
        server_socket.listen(100)
        while True:
            connection, address = server_socket.accept()
            while True:
                recv_data = connection.recv(8192)
                if not recv_data:  
                    break
                recv_data_list = recv_data.decode('utf-8').split(" ")
                com = recv_data_list[0]
                if com == "BEGIN":
                    self.TID += 1
                    self.IP_TID_DICT[address[0]] = self.TID
                    self.TID_SERVER_DICT[self.TID] = []
                    thr = threading.Thread(target=self.send_msg, args=(address[0], 9999, "TID:"+str(self.TID)))
                    thr.start()
                    if self.TID == 2:
                        t1 = threading.Thread(target=self.get_wait_graph)
                        t1.daemon = True
                        t1.start()
                elif com == "SET":
                    TID = self.IP_TID_DICT[address[0]]
                    server_node = recv_data_list[1].split(".")[0] 
                    obj = recv_data_list[1].split(".")[1]

                    server_ip = SERVER_IP_DICT[server_node]
                    if server_ip not in self.TID_SERVER_DICT[TID]:
                        self.TID_SERVER_DICT[TID].append(server_ip)

                    send_thr = threading.Thread(target=self.send_msg, 
                        args=(server_ip, 8888, str(TID)+":SET:"+obj+":"+recv_data_list[2]))
                    send_thr.start()

                elif com == "GET": 
                    TID = self.IP_TID_DICT[address[0]]
                    server_node = recv_data_list[1].split(".")[0]
                    obj = recv_data_list[1].split(".")[1]
                    server_ip = SERVER_IP_DICT[server_node]
                    if server_ip not in self.TID_SERVER_DICT[TID]:
                        self.TID_SERVER_DICT[TID].append(server_ip)

                    send_thr = threading.Thread(target=self.send_msg, 
                        args=(server_ip, 8888, str(TID)+":GET:"+obj))
                    send_thr.start()

                elif com == "COMMIT":
                    TID = self.IP_TID_DICT[address[0]]

                    if len(self.TID_SERVER_DICT[TID]) == 0:
                        client_ip = -1
                        for key, value in self.IP_TID_DICT.items():
                            if value == TID:
                                client_ip = key
                                break

                        thr = threading.Thread(target=self.send_msg, 
                            args=(client_ip, 9999, "Committed:" + str(TID)))
                        thr.start()

                    else:
                        self.TID_VOTES[TID] = []

                        multicast_thr = threading.Thread(target=self.multicast, 
                            args=(TID, str(TID)+":"+ "canCommit"))
                        multicast_thr.start()

                elif com == "VOTE":
                    TID = int(recv_data_list[1])
                    SID = recv_data_list[-1]

                    self.TID_VOTES[TID].append(SID)

                    client_ip = -1
                    for key, value in self.IP_TID_DICT.items():
                        if value == TID:
                            client_ip = key
                            break

                    if len(self.TID_VOTES[TID]) == len(self.TID_SERVER_DICT[TID]):
                        multicast_thr = threading.Thread(target=self.multicast, 
                            args=(TID, str(TID) + ":" + "doCommit"))
                        multicast_thr.start()


                        sendback_thr = threading.Thread(target=self.send_msg,
                            args=(client_ip, 9999, "Committed:"+str(TID)))
                        sendback_thr.start()

                elif com == "ABORT":
                    TID = self.IP_TID_DICT[address[0]]
                    self.TID_VOTES[TID] = []

                    multicast_thr = threading.Thread(target=self.multicast, 
                        args=(TID, str(TID) + ":" + "ABORT"))
                    multicast_thr.start()

                    return_thr = threading.Thread(target=self.send_msg, 
                        args=(address[0], 9999, "Aborted:" + str(TID)))
                    return_thr.start()

                elif com == "GOTTEN": 
                    TID = int(recv_data_list[1])
                    cmd = " ".join(recv_data_list[2:])

                    client_ip = -1
                    for key, value in self.IP_TID_DICT.items():
                        if value == TID:
                            client_ip = key
                            break

                    if cmd.split("=")[-1] == "NOT FOUND":
                        print('NOT FOUND')
                        self.TID_VOTES[TID] = []
                        multicast_thr = threading.Thread(target=self.multicast, 
                            args=(TID, str(TID) + ":" + "ABORT"))
                        multicast_thr.start()

                        send_thr = threading.Thread(target=self.send_msg, 
                            args=(client_ip, 9999, "NOT FOUND:" + str(TID)))
                        send_thr.start()

                    else:
                        send_thr = threading.Thread(target=self.send_msg,
                            args=(client_ip, 9999, "GOT:"+str(TID)+":"+cmd))
                        send_thr.start()

                elif com == "SETTED": 
                    TID = int(recv_data_list[1])
                    cmd = recv_data_list[2]

                    client_ip = -1
                    for key, value in self.IP_TID_DICT.items():
                        if value == TID:
                            client_ip = key
                            break

                    if cmd == 'OK':
                        send_thr = threading.Thread(target=self.send_msg,
                            args=(client_ip, 9999, "SETTED:"+str(TID)+":"+cmd))
                        send_thr.start()
                    if cmd == "Wait":
                        send_thr = threading.Thread(target=self.send_msg,
                            args=(client_ip, 9999, "SETTED:"+str(TID)+":"+cmd))
                        send_thr.start()


                elif com == 'WAIT_GRAPH':
                    if len(recv_data_list) == 1:
                        self.WAIT_GRAPH_REC.append("")
                    else:
                        self.WAIT_GRAPH_REC.append(recv_data_list[1])

                    if len(self.WAIT_GRAPH_REC) >= len(self.SERVER_LIST):
                        wait_graph_rec = deepcopy(self.WAIT_GRAPH_REC)
                        self.WAIT_GRAPH_REC = []
                        wait_graph = {}
                        for wait_info_long in wait_graph_rec:
                            for wait_info in wait_info_long.split("|"):
                                if wait_info != "":
                                    wait_graph = self.construct_graph(wait_info, wait_graph)

                        if len(wait_graph) != 0:
                            TID_dead = self.deadlock_detection(wait_graph)
                            if TID_dead:
                                multicast_thr = threading.Thread(target=self.multicast,
                                    args=(int(TID_dead), str(TID_dead) + ":" + "ABORT"))
                                multicast_thr.start()

                                client_ip = -1
                                for key, value in self.IP_TID_DICT.items():
                                    if value == int(TID_dead):
                                        client_ip = key
                                        break

                                return_thr = threading.Thread(target=self.send_msg,
                                    args=(client_ip, 9999, "Aborted:" + str(TID_dead)))
                                return_thr.start()

                elif com == "ONLINE":
                    if recv_data_list[1] not in self.SERVER_LIST:
                        self.SERVER_LIST.append(recv_data_list[1])
                else:
                    print("Coord received wrong msg "+ recv_data+"\n")

            connection.close()


    def deadlock_detection(self, wait_graph):
        path = []
        def dfs(v):
            path.append(v)
            for wait_for in wait_graph.get(v, []):
                if wait_for in path or dfs(wait_for):
                    return True
            path.remove(v)
            return False

        for v in wait_graph:
            if dfs(v):
                print('Deadlock Detected, abort TID:{}'.format(v))
                return v

        return False

    def construct_graph(self, wait_info, wait_graph):
        wait_info_list = wait_info.split(":")
        if wait_info_list[0] in wait_graph:
            for ele in wait_info_list[1].split(","):
                if ele not in wait_graph[wait_info_list[0]]:
                    wait_graph[wait_info_list[0]].append(ele)
        else:
            wait_graph[wait_info_list[0]] = wait_info_list[1].split(",")

        return wait_graph

    def get_wait_graph(self):
        while True:
            time.sleep(5)
            self.multicast_wait_graph("ALL:GET_WAIT_GRAPH")
                

if __name__ == '__main__': 
    host = socket.gethostbyname(socket.gethostname())
    coord_node = Coordinator(host, 8888)
    t1 = threading.Thread(target=coord_node.receive_cmd)
    t1.daemon = True
    t1.start()
    while True:
        pass