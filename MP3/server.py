import socket
import threading
import time
import sys
from copy import deepcopy

CORD_IP, CORD_PORT = ('172.22.94.168', 8888)

SERVER_IP_DICT = {
    'A':'172.22.156.159',
    'B':'172.22.158.159',
    'C':'172.22.94.169',
    'D':'172.22.156.160',
    'E':'172.22.158.160'
}

IP_SERVER_DICT = {
    '172.22.156.159':'A',
    '172.22.158.159':'B',
    '172.22.94.169':'C',
    '172.22.156.160':'D',
    '172.22.158.160':'E'
}

class Lock_Object(object):
    def __init__(self, TID, name, val):
        self.name = name
        self.ReadLock = []
        self.WriteLock = [TID]
        self.value = "NOT FOUND"
        self.value_tmp = {TID:val}
        self.theBufferList = BufferList()

    def read_val(self,TID):
        if TID in self.value_tmp.keys():
            return self.value_tmp[TID]
        else:
            return self.value

    def write_tmp_val(self, TID, val):
        self.value_tmp[TID] = val

    def set_lock(self, TID, lock):
        if lock == "read":
            self.ReadLock.append(TID)
        elif lock == "read_share":
            self.ReadLock.append(TID)
        elif lock == "write":
            self.WriteLock.append(TID)
        else:
            print("Error:set_lock \n")

    def unlock(self, TID, lock):
        if lock == "read":
            if TID in self.ReadLock:
                self.ReadLock.remove(TID)
        elif lock == "write":
            if TID in self.WriteLock:
                self.WriteLock.remove(TID)
        elif lock == "rw":
            if TID in self.ReadLock:
                self.ReadLock.remove(TID)
            if TID in self.WriteLock:
                self.WriteLock.remove(TID)
        else:
            print("Invalid Unlock Act\n")

    def promote_lock(self, TID):
        self.ReadLock.remove(TID)
        self.WriteLock.append(TID)


    def commit_value(self, TID):
        if TID in self.value_tmp.keys():
            self.value = self.value_tmp[TID]
            del self.value_tmp[TID]

    def delete_value_tmp(self, TID):
        if TID in self.value_tmp.keys():
            del self.value_tmp[TID]

class PendingAction(object):
    def __init__(self, tid, cmd, value, waitfor):
        self.tid = str(tid)
        self.cmd = cmd 
        self.value = value 
        self.waitfor = waitfor 

class BufferList(object):
    def __init__(self):
        self.ActionList = [] 
        self.TID_cnt = {} 

    def add_pending_action(self, TID, CMD, VAL, WAIT4):
        if WAIT4 == "inherit":
            wait4_parent = ""
            for entry in self.ActionList:
                if entry.tid == TID:
                    wait4_parent = entry.waitfor
                    break
            if wait4_parent == "":
                print("Error: Enter WailtList, Failed Inherit Wait4\n")
            else:
                self.ActionList.append(PendingAction(TID, CMD, VAL, wait4_parent))
                self.TID_cnt[TID] += 1
        elif WAIT4 == "RL-self" or WAIT4 == "WL": 
            self.ActionList.append(PendingAction(TID, CMD, VAL, WAIT4))
            self.TID_cnt[TID] = 1

        else:
            print("Error: Enter WailtList\n")

    def delete_TID(self, TID, host_name):
        indx_to_del = []
        for idx, entry in enumerate(self.ActionList):
            if entry.tid == TID:
                indx_to_del.append(idx)
        if len(indx_to_del) != 0:
            for indx in indx_to_del:
                self.ActionList[indx] = -1
            for _ in range(len(indx_to_del)):
                self.ActionList.remove(-1)
        if TID in self.TID_cnt.keys():
            del self.TID_cnt[TID]
        print(str(TID) + " deleted from " + host_name + "\n")


    def execute_pending(self, my_indx, entry, host_name, host_obj, host_Server):
        executed = False
        promoted = False
        pre_myTID_exist = False
        pre_waitfor = "NA"
        for i in range(my_indx): 
            if entry.tid == self.ActionList[i].tid:
                pre_myTID_exist = True
                pre_waitfor = self.ActionList[i].waitfor
                break

        if pre_myTID_exist:
            entry.waitfor = pre_waitfor

        else:  
            if entry.cmd == "SET":
                set_result = "NA"
                if len(host_obj.WriteLock) == 0 and len(host_obj.ReadLock) == 0:  
                    host_obj.set_lock(entry.tid, "write")
                    print("Writelock Set:" + host_name + "\n")
                    host_obj.write_tmp_val(entry.tid, entry.value)
                    executed = True
                    set_result = "OK"
  
                elif len(host_obj.WriteLock) == 0 and len(host_obj.ReadLock) != 0:  
                    if (entry.tid in host_obj.ReadLock) and (host_obj.check_RLock() == 1):  
                        host_obj.promote_lock(entry.tid)
                        host_obj.write_tmp_val(entry.tid, entry.value)
                        executed = True
                        promoted = True
                        set_result = "OK"
                    else:
                        entry.waitfor = "RL-self"
                        set_result = "Wait"


                elif len(host_obj.WriteLock) != 0 and len(host_obj.ReadLock) == 0:  
                    if entry.tid in host_obj.WriteLock:
                        host_obj.write_tmp_val(entry.tid, entry.value)
                        executed = True
                        set_result = "OK"
                    else:
                        entry.waitfor = "WL"
                        set_result = "Wait"

                else:
                    print("SET:Wrong lock for object: " + host_name + "\n")
                    print("ReadLock:"+" ".join(host_obj.ReadLock)+"\n")
                    print("WriteLock:" + " ".join(host_obj.WriteLock) + "\n")

                return_thr = threading.Thread(target=host_Server.send_msg,
                    args=(CORD_IP, CORD_PORT, "SETTED " + entry.tid + " " + set_result))
                return_thr.start()

            elif entry.cmd == "GET":
                SID = IP_SERVER_DICT[socket.gethostbyname(socket.gethostname())]
                val_get = "NA"

                if len(host_obj.WriteLock) == 0 and len(host_obj.ReadLock) == 0:  
                    val_get = host_obj.read_val(entry.tid) # value = "NOT FOUND" possible
                    if val_get == "NOT FOUND":
                        pass
                    else: 
                        host_obj.set_lock(entry.tid, "read")
                    executed = True

                elif len(host_obj.WriteLock) == 0 and len(host_obj.ReadLock) != 0:  
                    if entry.tid in host_obj.ReadLock:
                        val_get = host_obj.read_val(entry.tid)
                        executed = True
                    else:
                        host_obj.set_lock(entry.tid, "read_share")
                        val_get = host_obj.read_val(entry.tid)
                        executed = True

                elif len(host_obj.WriteLock) != 0 and len(host_obj.ReadLock) == 0:  
                    if entry.tid in host_obj.WriteLock:
                        val_get = host_obj.read_val(entry.tid)
                        executed = True
                    else:
                        entry.waitfor = "WL"
                        val_get = "Wait"
                else:
                    print("GET:Wrong lock for object: " + host_name + "\n")
                    print("ReadLock:" + " ".join(host_obj.ReadLock) + "\n")
                    print("WriteLock:" + " ".join(host_obj.WriteLock) + "\n")

                if val_get == "NOT FOUND":
                    host_obj.ReadLock=[]
                    host_obj.WriteLock=[]
                    host_obj.value_tmp = {}
                    executed = True
                    print("Corner Case: GET: NOT FOUND\n")
                return_thr = threading.Thread(target=host_Server.send_msg,
                    args=(CORD_IP, CORD_PORT, "GOTTEN " + entry.tid + " " + SID + "." + host_name + "=" + val_get))
                return_thr.start()
            else:
                print("Error wait action")
        return (executed, promoted)
    def check_all_pending(self, host_name, host_obj, host_Server):
        first_ready_indx = -1
        for idx, entry in enumerate(self.ActionList):
            RL_tmp = deepcopy(host_obj.ReadLock)
            WL_tmp = deepcopy(host_obj.WriteLock)
            if entry.waitfor == "RL-self":
                if entry.tid in RL_tmp:
                    RL_tmp.remove(entry.tid)
                if len(RL_tmp) == 0: 
                    first_ready_indx = idx
                    break
            elif entry.waitfor == "WL":
                if len(WL_tmp) == 0:
                    first_ready_indx = idx
                    break
        if first_ready_indx == -1: 
            print("WaitList No Entry is Ready")
            return

        executed_indx = []
        promoted_indx = []
        for indx in range(first_ready_indx, len(self.ActionList)):
            executed, promoted = self.execute_pending(indx, self.ActionList[indx], host_name, host_obj, host_Server)
            if executed:
                self.ActionList[indx].tid = -1
                executed_indx.append(indx)
            if promoted:
                promoted_indx.append(indx)
        if len(executed_indx) != 0:
            for indx in executed_indx:
                self.ActionList[indx] = -1
            for _ in range(len(executed_indx)):
                self.ActionList.remove(-1)
        else:
            print("WaitList No Entry Executed\n")

        if len(promoted_indx) != 0:
            for entry in self.ActionList:
                if entry.waitfor == "RL-self":
                    entry.waitfor = "WL"
            print("WaitList Promoted\n")
        else:
            print("WaitList No Promotion\n")
        self.TID_cnt = {}
        for entry in self.ActionList:
            if entry.tid not in self.TID_cnt:
                self.TID_cnt[entry.tid] = 1
            else:
                self.TID_cnt[entry.tid] += 1

    def get_wait_dict(self, host_obj): 
        if len(self.TID_cnt) == 0:
            return {}
        else:
            dic = {}
            for tid in self.TID_cnt.keys():
                wf = ""
                for entry in self.ActionList:
                    if entry.tid == tid:
                        wf = entry.waitfor
                        break
                if wf == "RL-self":
                    RL_self = deepcopy(host_obj.ReadLock)
                    RL_self = list(set(RL_self))
                    if tid in RL_self:
                        RL_self.remove(tid)
                    dic[tid] = RL_self
                elif wf == "WL":
                    WL = deepcopy(host_obj.WriteLock)
                    WL = list(set(WL))
                    dic[tid] = WL
                else:
                    print("Error:get_wait_dict\nwf="+wf+"\n")
            return dic

class Server(object):
    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.OBJ_DICT = {} 
        self.REQ_COMMIT_TID = [] 
        self.send_msg(CORD_IP, CORD_PORT,  "ONLINE " + str(host))

    def send_msg(self, host, port, cmd):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            s.connect((host, port))
        except:
            s.close()

        try:
            s.sendall(cmd.encode('utf-8'))
        except:
            s.close()

        s.close()

    def receive_cmd(self):
        server_socket= socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_socket.bind((self.host, self.port))
        server_socket.listen(100)
        while True:
            connection, address = server_socket.accept()

            while True:
                recv_data = connection.recv(2048)
                if not recv_data:
                    break

                recv_data_list = recv_data.decode('utf-8').split(":")
                TID = recv_data_list[0]
                com = recv_data_list[1]
                if com == "SET": 
                    obj = recv_data_list[2]
                    val = recv_data_list[3]
                    self.run_SET(TID, obj, val)

                elif com == "GET": 
                    obj = recv_data_list[-1]
                    self.run_GET(TID, obj)

                elif com == "canCommit":
                    self.REQ_COMMIT_TID.append(TID)

                    flag = True
                    for each_obj in self.OBJ_DICT.values():
                        if TID in each_obj.theBufferList.TID_cnt.keys():
                            flag = False
                            break

                    if flag: # Vote for the TID
                        self.REQ_COMMIT_TID.remove(TID)
                        self.send_msg(CORD_IP, CORD_PORT, 
                            "VOTE " + TID + " " + IP_SERVER_DICT[self.host])
                    else:
                        # debug #
                        print("Not Ready to Commit\n")

                elif com == "doCommit":
                    self.exe_doCOMMIT_ABORT("doCOMMIT", TID)

                elif com == "ABORT":
                    self.exe_doCOMMIT_ABORT("ABORT", TID)

                elif com == "GET_WAIT_GRAPH":
                    waitgraph_dic = {}
                    for each_obj in self.OBJ_DICT.values():
                        dic = each_obj.theBufferList.get_wait_dict(each_obj)
                        if len(dic) != 0:
                            for key, value in dic.items():
                                if key not in waitgraph_dic.keys():
                                    waitgraph_dic[key]=value
                                else: 
                                    waitgraph_dic[key] = list(set(waitgraph_dic[key]+value))

                    waitgraph_list = [] 
                    for key, value in waitgraph_dic.items():
                        waitgraph_list.append(key+':'+','.join(value))

                    waitgraph_string = '|'.join(waitgraph_list) 

                    self.send_msg(CORD_IP, CORD_PORT, "WAIT_GRAPH " + waitgraph_string)

                else:
                    print("server received wrong msg ")

            connection.close()


    def exe_doCOMMIT_ABORT(self, MODE, TID):
        if MODE == "doCOMMIT":
            for each_obj in self.OBJ_DICT.values():
                each_obj.commit_value(TID) 
                each_obj.unlock(TID, "rw") 
        elif MODE == "ABORT":
            for each_obj in self.OBJ_DICT.values():
                each_obj.delete_value_tmp(TID)
                each_obj.theBufferList.delete_TID(TID, each_obj.name)
                each_obj.unlock(TID, "rw")  

        for each_name, each_obj in self.OBJ_DICT.items():
            if len(each_obj.theBufferList.ActionList) != 0:
                each_obj.theBufferList.check_all_pending(each_name, each_obj, self)

        for req_TID in self.REQ_COMMIT_TID:
            flag = True
            for each_name, each_obj in self.OBJ_DICT.items():
                if req_TID in each_obj.theBufferList.TID_cnt.keys():
                    flag = False
                    break
            if flag:
                self.REQ_COMMIT_TID.remove(req_TID)
                self.send_msg(CORD_IP, CORD_PORT,
                    "VOTE " + req_TID + " " + IP_SERVER_DICT[self.host])


    def run_SET(self, TID, obj, val): 
        set_result = "NA"
        if obj not in self.OBJ_DICT.keys():
            self.OBJ_DICT[obj] = Lock_Object(TID, obj, val)
            set_result = "OK"

        else: 
            the_obj = self.OBJ_DICT[obj]
            if TID in the_obj.theBufferList.TID_cnt.keys() and the_obj.theBufferList.TID_cnt[TID] >= 1:
                the_obj.theBufferList.add_pending_action(TID, "SET", val, "inherit")
                set_result = "Wait"

            else: 
                if len(the_obj.WriteLock) == 0 and len(the_obj.ReadLock)== 0:
                    the_obj.set_lock(TID, "write")
                    the_obj.write_tmp_val(TID, val)
                    set_result = "OK"

                elif len(the_obj.WriteLock) == 0 and len(the_obj.ReadLock) != 0: 
                    if (TID in the_obj.ReadLock) and (len(the_obj.ReadLock) == 1): 
                        the_obj.promote_lock(TID)
                        the_obj.write_tmp_val(TID, val)
                        set_result = "OK"
                    else:
                        the_obj.theBufferList.add_pending_action(TID, "SET", val, "RL-self")
                        set_result = "Wait"

                elif len(the_obj.WriteLock) != 0 and len(the_obj.ReadLock) == 0:  # WriteLocked object
                    if TID in the_obj.WriteLock:
                        the_obj.write_tmp_val(TID, val)
                        set_result = "OK"
                    else:
                        the_obj.theBufferList.add_pending_action(TID, "SET", val, "WL")
                        set_result = "Wait"

                else:
                    print("Wrong lock")
        
        return_thr = threading.Thread(target=self.send_msg,
            args=(CORD_IP, CORD_PORT, "SETTED " + TID + " " + set_result))
        return_thr.start()

    def run_GET(self, TID, obj): 
        SID = IP_SERVER_DICT[self.host]
        val_get = "NA"

        if obj not in self.OBJ_DICT.keys():
            val_get = "NOT FOUND"

        else: 
            the_obj = self.OBJ_DICT[obj]
            if TID in the_obj.theBufferList.TID_cnt.keys() and the_obj.theBufferList.TID_cnt[TID] >= 1:
                the_obj.theBufferList.add_pending_action(TID, "GET", "--", "inherit")
                val_get = "Wait"

            else:  
                if len(the_obj.WriteLock) == 0 and len(the_obj.ReadLock) == 0:  
                    the_obj.set_lock(TID, "read")
                    val_get = the_obj.read_val(TID)

                elif len(the_obj.WriteLock) == 0 and len(the_obj.ReadLock) != 0:  
                    if TID in the_obj.ReadLock:
                        val_get = the_obj.read_val(TID)
                    else:
                        the_obj.set_lock(TID, "read_share")
                        val_get = the_obj.read_val(TID)

                elif len(the_obj.WriteLock) != 0 and len(the_obj.ReadLock) == 0:
                    if TID in the_obj.WriteLock:
                        val_get = the_obj.read_val(TID)
                    else:
                        val_get = "Wait"
                        the_obj.theBufferList.add_pending_action(TID, "GET", "--", "WL")

                else:
                    print("Wrong lock for object: " + obj + "\n")

        return_thr = threading.Thread(target=self.send_msg,
            args=(CORD_IP, CORD_PORT, "GOTTEN " + TID + " " + SID + "." + obj + "=" + val_get))
        return_thr.start()



if __name__ == '__main__':
    host = socket.gethostbyname(socket.gethostname())
    server_node = Server(host, 8888)
    t1 = threading.Thread(target=server_node.receive_cmd)
    t1.daemon = True
    t1.start()
    while True:
        pass
