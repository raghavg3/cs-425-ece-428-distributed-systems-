import time
import socket
import threading
import sys
import json
import hashlib
from collections import defaultdict
import netifaces as ni # need pip3 install

GROUP_ADDR = ['172.22.94.168', '172.22.156.159','172.22.158.159','172.22.94.169','172.22.156.160',
                '172.22.158.160','172.22.94.170','172.22.156.161','172.22.158.161','172.22.94.171']  # list for addresses

ADDR_ID_DICT = {'172.22.94.168':1,          # hash map for addresses
                '172.22.156.159':2,
                '172.22.158.159':3,
                '172.22.94.169':4,
                '172.22.156.160':5,
                '172.22.158.160':6,
                '172.22.94.170':7,
                '172.22.156.161':8,
                '172.22.158.161':9,
                '172.22.94.171':10
                }
class Application:
    def __init__(self, name, port, group_num):
        self.name = name 
        self.group_num = int(group_num)
        self.port = int(port)
        self.vector_timestamp = [0] * int(group_num)
        self.holdback_queue = []
        self.received_msg = set()
        self.sending_group = []
        self.ip = ni.ifaddresses('eth0')[ni.AF_INET][0]['addr']
        self.process_idx = ADDR_ID_DICT[self.ip]
        self.ADDR_NAME_DICT = defaultdict()
        self.PINGACK_DICT = defaultdict()

    def run(self):
        init_send_thread = threading.Thread(target=self.init_send_connection)   # thread for sending
        init_recv_thread = threading.Thread(target=self.init_recv_connection)   # thread for receiving
        init_recv_thread.start()  
        init_send_thread.start()
        init_recv_thread.join()
        init_send_thread.join()
        self.broadcast(json.dumps({'Type':'Init', 'Content':self.name}).encode('utf-8'))   #broadcast initially 
        print("READY")
        threading.Thread(target=self.init_fail_detection).start()    
        while True:
            try:
                message = sys.stdin.readline().strip()
                msg_hash = hashlib.sha256(message.encode('utf-8')).hexdigest()     # encrypt data
                self.received_msg.add(msg_hash)
                self.vector_timestamp[self.process_idx - 1] += 1
                
                # dump messages and timestamps to a json file 
                
                message = json.dumps({'Type':'Message', 'Content':message, 'Timestamp':','.join([str(x) for x in self.vector_timestamp])}).encode('utf-8')    
                self.broadcast(message)
                
                # send json file to all nodes
                
            except:
                sys.exit(0)

    def broadcast(self, message):           # function for broadcasting
        for c in self.sending_group:
            try:
                c.send(message)
            except:
                continue

    def init_send_connection(self):           # connect  to node and send  packets
        i = 0
        while True:
            try:
                if i == self.group_num:
                    break
                send_s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                send_s.connect((GROUP_ADDR[i], self.port))
                print('# Successfully connect to {}'.format(GROUP_ADDR[i]))
                self.sending_group.append(send_s)
                i += 1
            except socket.error as msg:
                continue

    def init_recv_connection(self):          # receive packets
        recv_s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        recv_s.bind((self.ip, self.port))
        recv_s.listen(self.group_num)
        i = 0
        while True:
            try:
                if i == self.group_num:
                    break
                client_socket, addr = recv_s.accept()
                print('# Successfully recev from {}'.format(addr[0]))
                threading.Thread(target=self.listenToClient, args=(client_socket, addr[0], ADDR_ID_DICT[addr[0]])).start()
                i += 1
            except:
                continue

    def init_fail_detection(self):
        while True:
            try:
                time.sleep(0.05)                                                        # 0.05 seconds delay
                self.broadcast(json.dumps({'Type':'Ping'}).encode('utf-8'))
                now = time.time()
                for addr, t in self.PINGACK_DICT.items():
                    if now - t > 0.2:                                                   # 0.2 seconds delta
                        print('{} has left'.format(self.ADDR_NAME_DICT[addr]))
                        del self.PINGACK_DICT[addr]
            except:
                continue

    def listenToClient(self, client_socket, client_addr, client_id):
        size = 2048
        while True:
            try:
                received_data = client_socket.recv(size)
                if received_data:
                    data = json.loads(received_data.decode('utf-8'))
                    if data['Type'] == 'Ping' and client_addr != self.ip:
                        self.PINGACK_DICT[client_addr] = time.time()
                        
                    elif data['Type'] == 'Message':
                        msg, timestamp = data['Content'], [int(x) for x in data['Timestamp'].split(',')]
                        msg_hash = hashlib.sha256(msg.encode('utf-8')).hexdigest()
                        if msg_hash not in self.received_msg:
                            if self.compare_timestamp(timestamp, client_id):
                                self.received_msg.add(msg_hash)
                                self.vector_timestamp[client_id - 1] = timestamp[client_id - 1]
                                self.broadcast(received_data)
                                print('{}: {}'.format(self.ADDR_NAME_DICT[client_addr], msg))
                                self.scan_holdback_queue()
                            else:
                                print('#Put into holdback q', data, client_id)
                                self.holdback_queue.append((msg, timestamp, client_id))
                    elif data['Type'] == 'Init':
                        self.ADDR_NAME_DICT[client_addr] = data['Content']
            except:
                continue

    def scan_holdback_queue(self):                                                      # check holdback queue
        while len(self.holdback_queue) > 0:
            if any([compare_timestamp(t) for m, t, c in self.holdback_queue]):
                i = [compare_timestamp(t) for m, t in self.holdback_queue].index(True)
                m, t, c = self.holdback_queue.pop(i)
                msg_hash = hashlib.sha256(m.encode('utf-8')).hexdigest()
                self.received_msg.add(msg_hash)
                print(m)
                self.vector_timestamp[c - 1] = t[c - 1]
            else:
                break

    def compare_timestamp(self, recv_timestamp, client_id):                    # function to compare timestamp 
        i = 0
        for x, y in zip(recv_timestamp, self.vector_timestamp):
            if i == client_id - 1:
                if x != y+1:
                    return False
            if i != client_id - 1:
                if x > y:
                    return False
            i += 1
        return True

if __name__ == '__main__':
    if len(sys.argv) != 4:
        print("Usage: python36 mp1.py name port n")
        sys.exit(0)
    a = Application(sys.argv[1], sys.argv[2], sys.argv[3])
    a.run()
